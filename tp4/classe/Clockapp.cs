using System;
using System.Collections.Generic;


namespace ClockApp
{
    class ClockApp
    {
        public static void RunClockApp(){
            ClockApp clockApp=new ClockApp();
            clockApp.Run();
        }
        private List<Clock> clockList = new List<Clock>();

        public void Run()
        {
            int choice;

            do
            {
                DisplaySubMenu();
                choice = GetSubMenuChoice();
                Console.Clear();

                switch (choice)
                {
                    case 1:
                        ViewActiveClocks();
                         Console.ReadKey();
                            Console.Clear(); 
                        break;
                    case 2:
                        AddClock();
                         Console.ReadKey();
                            Console.Clear(); 
                        break;
                    case 3:
                        Console.WriteLine("> Retour au menu principal <");
                         Console.ReadKey();
                            Console.Clear(); 
                        break;
                    default:
                        Console.WriteLine("invalide. Veuillez réessayer ");
                        break;
                }

            } while (choice != 3);
        }

        private void DisplaySubMenu()

        {
            Console.WriteLine("""
            
             Choisisssez 
            
             1) Voir les Horloges actives 
             2) Ajouter une horloge 
             3) Retour au menu principale 
            
            
            """);
            Console.Write("---- Choix ==  ");
        
        }

        private int GetSubMenuChoice()
        {
            int choice;
            while (!int.TryParse(Console.ReadLine(), out choice))
            {
                Console.WriteLine(" INVALIDE. Veuillez entrer un nombre ");
                Console.Write("---- Choix ==  ");
               
            }
            return choice;
        }

        private void ViewActiveClocks()
        {
            
            Console.WriteLine("Liste des horloges :");
            DisplayCurrentTime();
            foreach (var clock in clockList)
            {
                Console.WriteLine(clock.ToString());
            }

            
             
        }

        private void DisplayCurrentTime()
        {
            // Afficher le temps actuel (peut être mis à jour en temps réel)
            Console.WriteLine($"""
            Antananarivo ==
            {DateTime.Now.ToString("HH:mm:ss")}
            {DateTime.Now.ToString("ddd dd MMM")}
            """);
            
        }

        private void AddClock()
        {
            Console.WriteLine("------ Choisissez une ville --------");
            Console.Write("""
                Moscou, Dubaï, Mexique ,Japon,Argentine,Australie,Canada
                -----(veuillez a ne pas faire de faute sur le nom)------
                ---->
                """);
            string city = Console.ReadLine() ?? "";

            Clock newClock = new Clock(city);
            clockList.Add(newClock);

            Console.WriteLine("----- Votre horloge a bien été enregistree-----)");
            
        }
    }

    class Clock
    {
        public string City { get; set; }
        public int TimeDifference { get; set; } // Différence par rapport à l'heure locale

        public Clock(string city)
        {
            City = city;
            // Vous pouvez ajuster les différences de temps en fonction du fuseau horaire de chaque ville
            TimeDifference = GetTimeDifference(city);
        }

        private static int GetTimeDifference(string city)
        {
            return city.ToLower() switch
            {
                "moscou" => 3,
                "dubaï" => 4,
                "mexique" => -6,
                "japon" =>9,
                "Argentine" =>-3,
                "Australie" =>10,
                "Canada" =>-7,
                _ => 0,// Par défaut, aucune différence
            };
        }

        public override string ToString()

        {
            DateTime localTime=DateTime.UtcNow.AddHours(TimeDifference);
            string timeString= localTime.ToString("HH:mm");
            return $"""
            Autre horloge 
            {City} - {timeString} (GMT{(TimeDifference >= 0 ? "+" : "")}{TimeDifference} / {localTime}) 
            ---------------------------------------------------------------------------------
            """;
        }
    }

}