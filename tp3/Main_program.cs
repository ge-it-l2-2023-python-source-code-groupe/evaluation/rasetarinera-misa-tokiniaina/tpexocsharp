using System;

using Function;



namespace Tp3
{
    class Main_program

    {

        internal static void Program()
        {
            Console.WriteLine("");
            Console.WriteLine("""
            -----------Tp_N_3_Csarp-----------
            """);

            bool program = true;
            while (program != false)
            {
                Console.WriteLine(
                   """
                 |---------------------------------|
                 |1) Nouvelle manche               |
                 |2) Voir l'historique des manches |
                 |3) Quitter le programme          |
                 |---------------------------------|
                """
                   );
                Console.Write("Votre choix  \n  -------> : ");
                int userChoice = Int32.Parse(Console.ReadLine() ?? "");

                switch (userChoice)
                {
                    case 1:
                        Console.WriteLine("------------NOUVELLE PARTIE-----------");
                        Function.Newgame.NewPlayer();
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 2:
                        Console.WriteLine("----------------HISTORIQUE--------------------");
                        Console.WriteLine("***LE TEMPS SEULEMENT***");
                        Function.Newgame.History();
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        Console.WriteLine("----VOUS AVEZ QUITTEZ-----");
                        Console.ReadKey();
                        Console.Clear();
                        program = false;

                        break;
                    default:
                        Console.WriteLine("UNE ERREUR S'EST PRODUIT ");
                        break;
                }
            }



        }

    }
}






















