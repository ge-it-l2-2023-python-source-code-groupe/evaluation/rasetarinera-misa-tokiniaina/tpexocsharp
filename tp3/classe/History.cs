using System;
using System.Collections.Generic;

namespace Historique
{
    public class GameRound
    {
        public DateTime RoundDateTime { get; set; }
        public Dictionary<string, int> PlayerScores { get; set; }
        public Dictionary<string, int> PointsEarned { get; set; }

        public GameRound()
        {
            RoundDateTime = DateTime.Now;
            PlayerScores = new Dictionary<string, int>();
            PointsEarned = new Dictionary<string, int>();
        }
    }
    
}